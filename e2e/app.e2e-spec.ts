import { PDSBackOfficePage } from './app.po';

describe('pds-back-office App', function() {
  let page: PDSBackOfficePage;

  beforeEach(() => {
    page = new PDSBackOfficePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
