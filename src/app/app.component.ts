import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';
import {CategoryService} from './services/category.service';

@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
  directives: [
    ROUTER_DIRECTIVES,
  ],
  providers: [CategoryService]
})
export class AppComponent {
  title = 'app works!';
}
