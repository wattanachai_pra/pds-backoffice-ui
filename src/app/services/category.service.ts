import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class CategoryService {
  private baseUrl: string = 'http://www.mocky.io/v2/57c6e015120000ab06c8ebd8';
  private baseUrlCreate: string = 'http://www.mocky.io/v2/57c7c1481200008d1dc8ecba';

  constructor(private http: Http) { }

  getCategoryById(id: number) {
    const categories$ = this.http
      .get(`${this.baseUrl}/api/v1/categories/${id}`)
      .map((res: Response) => res.json()).catch(this.handleError);
    return categories$;
  }

  createCategory(category: any) {
    // let headers = new Headers({
    //   'Content-Type': 'application/json',
    // });
    const categoryRsult = this.http
      .post(`${this.baseUrlCreate}/api/v1/categories`, JSON.stringify(category)).map(res => res.json());
    return categoryRsult;
  }

  handleError(error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return Observable.throw(errMsg);
  }
}
