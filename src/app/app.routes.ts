import { provideRouter, RouterConfig } from '@angular/router';
import { TabsComponent } from './components/tabs/tabs.component';


export const routes: RouterConfig = [
    { path: '', component: TabsComponent },
    { path: 'pds', component: TabsComponent }
];

export const APP_ROUTER_PROVIDERS = [
    provideRouter(routes)
];
