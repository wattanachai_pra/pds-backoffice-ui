import { Component, OnInit, EventEmitter } from '@angular/core';
import { Product} from '../../models/product';

@Component({
  selector: 'price-display',
  inputs: ['price'],
  template: `
   <div>
    <h4>price :  \${{ price }}</h4>
  </div>
  `
})
class PriceDisplay {
  price: number;
}


@Component({
  selector: 'product-row',
  inputs: ['product'],
  host: {'class': 'item'},
  directives: [PriceDisplay],
  template: `
  <div>
    <h2>{{ product.name }}</h2>
  </div>
  <price-display [price]="product.price"></price-display>
  `
})
class ProductRow {
  product: Product;
}


@Component({
  selector: 'products-list',
  directives: [ProductRow],
  inputs: ['productList'],
  outputs: ['onProductSelected'],
  template: `
  <div>
    <product-row 
      *ngFor="let myProduct of productList" 
      [product]="myProduct" 
      (click)='clicked(myProduct)'
      [class.selected]="isSelected(myProduct)">
    </product-row>
  </div>
  `
})
class ProductsList {
  productList: Product[];
  onProductSelected:any ;
  currentProduct: Product;
  constructor() {
    this.onProductSelected = new EventEmitter();
  }

  clicked(product: Product): void {
    this.currentProduct = product;
    this.onProductSelected.emit(product);
  }

  isSelected(product: Product): boolean {
    if (!product || !this.currentProduct) {
      return false;
    }
    return product.name === this.currentProduct.name;
  }
}

@Component({
  moduleId: module.id,
  selector: 'app-product',
  directives: [ProductsList],
  templateUrl: 'product.component.html',
  styleUrls: ['product.component.css']
})
export class ProductComponent {

  products: Product[];

  constructor() {
    this.products = [
      new Product(
        { id: 1, name: 'product1' , price: 500 }),
      new Product(
        { id: 2, name: 'product2' , price: 900 }),
      new Product(
        { id: 3, name: 'product3', price: 1000 })
    ];
  console.log( this.products);
  }

  productWasSelected(product: Product): void {
    console.log('Product clicked: ', product);
  }
}