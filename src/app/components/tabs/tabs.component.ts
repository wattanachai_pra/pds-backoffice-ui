import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TAB_DIRECTIVES, TabsetComponent, TabDirective } from 'ng2-bootstrap/ng2-bootstrap';
import { CategoryComponent } from '../category/category.component';
import { ProductComponent } from '../product/product.component';


@Component({
  moduleId: module.id,
  selector: 'app-tabs',
  directives: [
    TAB_DIRECTIVES,
    CategoryComponent,
    ProductComponent
  ],
  templateUrl: 'tabs.component.html',
  styleUrls: ['tabs.component.css']
})

export class TabsComponent implements OnInit {
  @ViewChild(CategoryComponent) categoryComponent: CategoryComponent;
  @ViewChild(ProductComponent) productComponent: ProductComponent;
  @ViewChild(TabsetComponent) tabset: TabsetComponent


  constructor(
    private route: ActivatedRoute
  ) { }

  state = 1;
  public tabList: any = ['Category', 'Product'];
  private previousActiveTab: string = 'Category';

  ngOnInit() {
    let sub = this.route.params.subscribe(params => {
      let currentTab = params['mainTab'];
      if (currentTab) {
        this.previousActiveTab = currentTab;
        this.setCurrentTabByName(currentTab);
      }
    });
  }

  ngOnDestroy() {

  }

  selectTab(state) {
    this.state = state;
  }

  desClickHandle(e) {

  }

  public setCurrentTabByName(name: string): void {
    for (let i = 0; i < this.tabList.length; i++) {
      let tabName = this.tabList[i];
      this.tabset.tabs[i].active = (name == tabName);
    }
  }

  public select(tab: TabDirective, tabName: string) {
    if (tabName !== this.previousActiveTab) {
      this.previousActiveTab = tabName;
    }
  }

}
