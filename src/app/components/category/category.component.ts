import { Component, OnInit } from '@angular/core';
import { Category} from '../../models/category';
import { AlertComponent, TOOLTIP_DIRECTIVES } from 'ng2-bootstrap/ng2-bootstrap';
import { NgForm } from '@angular/forms';
import { CategoryService } from '../../services/category.service';

@Component({
  moduleId: module.id,
  selector: 'app-category',
  templateUrl: 'category.component.html',
  styleUrls: ['category.component.css'],
  directives: [
    AlertComponent,
    TOOLTIP_DIRECTIVES
  ]
})
export class CategoryComponent implements OnInit {
  category: Category
  errorMessage: string;
  constructor(private categoryService: CategoryService) {
  }

  reset() {
    this.category.name = '';
  }

  createCategory() {
    console.log(this.category);
    this.categoryService.createCategory(this.category).subscribe(c => {
      this.category.id = c.id;
      this.category.name = c.name;
    }, error => this.errorMessage);
  }

  ngOnInit() {
    this.category = new Category();
    this.categoryService
      .getCategoryById(1)
      .subscribe(c => {
        this.category.id = c.id;
        this.category.name = c.name;
      }, error => this.errorMessage = <any>error);
  }
}