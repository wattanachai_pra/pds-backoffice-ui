/* tslint:disable:no-unused-variable */

import { addProviders } from '@angular/core/testing';
import {Category} from './category';
import {beforeEach, describe, expect, it} from '@angular/core/testing';

//describe() declares a test suite (a group of tests) 
describe('Test Category Model', () => {
  //it() declares a test

  it('should construct a Category Correctly', () => {
    const productObj = {
      id: 1,
      name: 'product name'
    }
    const categoryObj = {
      id: 1,
      name: 'Category Name',
      product: productObj
    }
    const category = new Category(categoryObj);
    //expect() declares an assertion
    expect(category.id).toBe(1);
    expect(category.name).toBe('Category Name');
  });
});

