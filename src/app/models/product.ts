export class Product {
    constructor(product?: any) {
        this.id = product && product.id ? product.id : undefined;
        this.name = product && product.name ? product.name : undefined;
        this.price = product && product.price ? product.price : undefined;
    }
    id: number;
    name: string;
    price: number;
}
