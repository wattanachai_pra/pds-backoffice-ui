export class Category {
    constructor(category?: any) {
        this.id = category && category.id ? category.id : undefined;
        this.name = category && category.name ? category.name : undefined;
    }
    id: number;
    name: string;
}
